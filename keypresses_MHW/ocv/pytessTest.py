import cv2 as cv
import pytesseract
import numpy as np

from random import randint
from pytesseract import Output
import re


def border(src):
    borderType = cv.BORDER_CONSTANT
    window_name = "copyMakeBorder Demo"

    imageName = 'a.jpg'
    # Loads an image
    # Check if image is loaded fine
    #cv.namedWindow(window_name, cv.WINDOW_AUTOSIZE)

    top = int(0.05 * src.shape[0])  # shape[0] = rows
    bottom = top
    left = int(0.05 * src.shape[1])  # shape[1] = cols
    right = left

    value = [0, 0, 0]

    dst = cv.copyMakeBorder(src, top, bottom, left, right, borderType, None, value)

    #cv.imshow(window_name, dst)

    return dst

def original():
    pytesseract.pytesseract.tesseract_cmd = r"C:\Programme\Tesseract-OCR\tesseract.exe"
    # Grayscale, Gaussian blur, Otsu's threshold
    test0 = cv.imread('testdata/test0.png')
    image = cv.imread('pytess/1.png')
    gray = cv.cvtColor(test0, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    # Morph open to remove noise and invert image
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (3, 3))
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=1)
    invert = 255 - opening

    # Perform text extraction
    data = pytesseract.image_to_string(invert, lang='eng', config='--psm 6')
    print(data)

    cv.imshow('thresh', thresh)
    cv.imshow('opening', opening)
    cv.imshow('invert', invert)
    cv.waitKey()


pytesseract.pytesseract.tesseract_cmd = r"C:\Programme\Tesseract-OCR\tesseract.exe"

# 867,881
# 922,881

# 922,909

tests = []
for index in range(11):
    PATH = 'testdata/test' + str(index) + '.png'
    test = cv.imread(PATH)
    tests.append(test)

bgrimages = []
for test in tests:
    cut1 = np.array(test)
    img = cut1[881:909, 867:964, :]

    scale_percent = 300  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv.resize(img, dim, interpolation=cv.INTER_AREA)

    imgasCV = cv.cvtColor(resized, cv.COLOR_RGB2BGR)
    #cv.imshow('', imgasCV)
    #cv.waitKey(0)
    #border(imgasCV)

    bgrimages.append(border(imgasCV))

for img in bgrimages:
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    # Morph open to remove noise and invert image
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (3, 3))
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=1)
    invert = 255 - opening

    custom_config = r'--oem 3 --psm 6 outputbase digits'

    # Perform text extraction
    d = pytesseract.image_to_string(invert, config=custom_config)

    print(type(d))
    #data = pytesseract.image_to_string(invert, lang='eng', config='--oem 0')
    print(d)

    #cv.imshow('thresh', thresh)
    #cv.imshow('opening', opening)
    #cv.imshow('invert', invert)
    #cv.waitKey()

