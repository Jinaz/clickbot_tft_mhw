import keyboard
import time
import glob

import pyautogui
import cv2 as cv
import pytesseract
import numpy as np

import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const
import TFT.ocv.TFT_cvf_utils as cv_util


def load_champs_from_data(path):
    img = cv.imread(path)
    image_as_array = np.array(img)

    image1 = image_as_array[const.CHAMPS_SIZE[0][0]:const.CHAMPS_SIZE[0][1],
             const.CHAMPS_SIZE[0][2]:const.CHAMPS_SIZE[0][3], :]
    image2 = image_as_array[const.CHAMPS_SIZE[1][0]:const.CHAMPS_SIZE[1][1],
             const.CHAMPS_SIZE[1][2]:const.CHAMPS_SIZE[1][3], :]
    image3 = image_as_array[const.CHAMPS_SIZE[2][0]:const.CHAMPS_SIZE[2][1],
             const.CHAMPS_SIZE[2][2]:const.CHAMPS_SIZE[2][3], :]
    image4 = image_as_array[const.CHAMPS_SIZE[3][0]:const.CHAMPS_SIZE[3][1],
             const.CHAMPS_SIZE[3][2]:const.CHAMPS_SIZE[3][3], :]
    image5 = image_as_array[const.CHAMPS_SIZE[4][0]:const.CHAMPS_SIZE[4][1],
             const.CHAMPS_SIZE[4][2]:const.CHAMPS_SIZE[4][3], :]

    return [image1, image2, image3, image4, image5]


def get_champs():
    image = pyautogui.screenshot()
    image_as_array = np.array(image)

    image1 = image_as_array[const.CHAMPS_SIZE[0][0]:const.CHAMPS_SIZE[0][1],
             const.CHAMPS_SIZE[0][2]:const.CHAMPS_SIZE[0][3], :]
    image2 = image_as_array[const.CHAMPS_SIZE[1][0]:const.CHAMPS_SIZE[1][1],
             const.CHAMPS_SIZE[1][2]:const.CHAMPS_SIZE[1][3], :]
    image3 = image_as_array[const.CHAMPS_SIZE[2][0]:const.CHAMPS_SIZE[2][1],
             const.CHAMPS_SIZE[2][2]:const.CHAMPS_SIZE[2][3], :]
    image4 = image_as_array[const.CHAMPS_SIZE[3][0]:const.CHAMPS_SIZE[3][1],
             const.CHAMPS_SIZE[3][2]:const.CHAMPS_SIZE[3][3], :]
    image5 = image_as_array[const.CHAMPS_SIZE[4][0]:const.CHAMPS_SIZE[4][1],
             const.CHAMPS_SIZE[4][2]:const.CHAMPS_SIZE[4][3], :]

    cvt_image1 = cv.cvtColor(image1, cv.COLOR_RGB2BGR)
    cvt_image2 = cv.cvtColor(image2, cv.COLOR_RGB2BGR)
    cvt_image3 = cv.cvtColor(image3, cv.COLOR_RGB2BGR)
    cvt_image4 = cv.cvtColor(image4, cv.COLOR_RGB2BGR)
    cvt_image5 = cv.cvtColor(image5, cv.COLOR_RGB2BGR)

    return [cvt_image1, cvt_image2, cvt_image3, cvt_image4, cvt_image5]


def get_champs_save(path, index):
    champs = get_champs()
    for c in champs:
        cv_util.save_img(path, str(index) + '.png', c)
        index += 1

    return index


def get_index(class_champs):
    champs = get_champs()
    res = []

    for i in range(len(champs)):
        for img in class_champs:
            if cv_util.compare_images(champs[i], img, const.BUY_CHAMP_M_TH, const.BUY_CHAMP_S_TH):
                res.append(i)

    return res


def is_loading(template):
    curr = pyautogui.screenshot()
    curr_as_array = np.array(curr)
    cvt_curr = cv.cvtColor(curr_as_array, cv.COLOR_RGB2BGR)
    res = cv_util.compare_images(template, cvt_curr, const.LOADING_M_TH, const.LOADING_S_TH)
    return res


def is_planing(template):
    curr = pyautogui.screenshot()
    cut_curr = cv_util.cut_img(curr, const.PLANING_SIZE[0], const.PLANING_SIZE[1], const.PLANING_SIZE[2], const.PLANING_SIZE[3])
    cvt_curr = cv_util.color_convert(cut_curr)
    res = cv_util.compare_images(template, cvt_curr, const.PLANING_M_TH, const.PLANING_S_TH)
    return res


def get_coins(config=r'--psm 13 --oem 3 -c tessedit_char_whitelist=0123456789'):
    img = cv_util.screen_shot()
    img_cut = cv_util.cut_img(img, const.COIN_SIZE[0], const.COIN_SIZE[1], const.COIN_SIZE[2], const.COIN_SIZE[3])
    converted_img = cv_util.color_convert(img_cut)
    gray = cv.cvtColor(converted_img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    # Perform text extraction
    d = pytesseract.image_to_string(thresh, config=config)
    if d:
        return int(d)
    else:
        return -1


def get_level(config=r'--psm 13 --oem 3 -c tessedit_char_whitelist=0123456789'):
    img = cv_util.screen_shot()
    img_cut = cv_util.cut_img(img, const.LEVEL_SIZE[0], const.LEVEL_SIZE[1], const.LEVEL_SIZE[2], const.LEVEL_SIZE[3])
    converted_img = cv_util.color_convert(img_cut)
    gray = cv.cvtColor(converted_img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    # Perform text extraction
    d = pytesseract.image_to_string(thresh, config=config)
    if d:
        return int(d)
    else:
        return -1


def get_level_test(img, config=r'--psm 13 --oem 3 -c tessedit_char_whitelist=0123456789'):
    img_cut = cv_util.cut_img(img, const.LEVEL_SIZE[0], const.LEVEL_SIZE[1], const.LEVEL_SIZE[2], const.LEVEL_SIZE[3])
    converted_img = cv_util.color_convert(img_cut)
    gray = cv.cvtColor(converted_img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    # Perform text extraction
    d = pytesseract.image_to_string(thresh, config=config)
    if d:
        return int(d)
    else:
        return -1


if __name__ == "__main__":

    pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
    # tess_config = r'--psm 13 --oem 3 -c tessedit_char_whitelist=0123456789'
    # while True:
    #     try:  # used try so that if user pressed other than the given key error will not be shown
    #         if keyboard.is_pressed('space'):
    #             num = get_coins(tess_config)
    #             print(num)
    #             time.sleep(1)
    #
    #         if keyboard.is_pressed('enter'):
    #             break
    #
    #     except:  # if user pressed a key other than the given key the loop will break
    #         break

    # path2 = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//1_Star_Full//Data//*.png'
    # for full_path in glob.glob(path2):
    #     filename = full_path[86:]
    #     print(filename)
    #     level = get_level_test(cv_util.load_img(full_path))
    #     print(level)

