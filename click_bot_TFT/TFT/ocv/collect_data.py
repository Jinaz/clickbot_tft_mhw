import keyboard
import time
import numpy as np
import glob

from TFT.utils.MouseConnectors import Mouse
import TFT.utils.MouseControl as mc

import TFT.ocv.TFT_cvf_utils as cv_util
import TFT.ocv.TFT_cvf as cvf

import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const


def get_champ_info(mouse, path, filename):
    mc.mouse_drag(mouse, coord.CHAMPS_ON_BOARD[0], coord.CHECK_CHAMP)

    mc.mouse_click(mouse, False, coord.CHECK_CHAMP)

    screen_shot = cv_util.screen_shot()
    cut_screen_shot = cv_util.cut_img(screen_shot, const.CHAMP_INFO_SIZE[0], const.CHAMP_INFO_SIZE[1],
                                      const.CHAMP_INFO_SIZE[2], const.CHAMP_INFO_SIZE[3])
    cvt_screen_shot = cv_util.color_convert(cut_screen_shot)
    mc.mouse_drag(mouse, coord.CHECK_CHAMP, coord.CHAMPS_ON_BOARD[0])

    cv_util.save_img(path, filename, cvt_screen_shot)


def get_champ_info_full_size(mouse, path, filename):
    mc.mouse_drag(mouse, coord.CHAMPS_ON_BOARD[0], coord.CHECK_CHAMP)

    mc.mouse_click(mouse, False, coord.CHECK_CHAMP)

    screen_shot = cv_util.screen_shot()
    image_as_array = np.array(screen_shot)
    cvt_screen_shot = cv_util.color_convert(image_as_array)
    mc.mouse_drag(mouse, coord.CHECK_CHAMP, coord.CHAMPS_ON_BOARD[0])

    cv_util.save_img(path, filename, cvt_screen_shot)


def collect_data():
    index1 = 0
    index2 = 0
    m = Mouse()
    mc.alt_tab()

    while True:
        try:  # used try so that if user pressed other than the given key error will not be shown
            if keyboard.is_pressed('z'):
                filename = str(index1) + '.png'
                get_champ_info(m, 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champ_Info//',
                               filename)
                index1 += 1

            if keyboard.is_pressed('x'):
                filename = str(index1) + '.png'
                img = cv_util.screen_shot()
                image_as_array = np.array(img)
                cvt = cv_util.color_convert(image_as_array)
                cv_util.save_img(
                    'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champ_Info_Full//',
                    filename, cvt)
                index1 += 1

            if keyboard.is_pressed('space'):
                index2 = cvf.get_champs_save(
                    'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champs//', index2)

            if keyboard.is_pressed('enter'):
                break

        except:  # if user pressed a key other than the given key the loop will break
            break


def preprocessing_champ_info(path, path_save, num):
    for full_path in glob.glob(path):
        filename = full_path[num:]
        print(filename)
        img = cv_util.load_img(full_path)
        img_cut = cv_util.cut_img(img, const.CHAMP_INFO_SIZE[0], const.CHAMP_INFO_SIZE[1],
                        const.CHAMP_INFO_SIZE[2], const.CHAMP_INFO_SIZE[3])
        cv_util.save_img(path_save, filename, img_cut)


if __name__ == "__main__":
    # pass
    collect_data()

    # path1 = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//1_Star_Full//Data//*.png'
    # path1_save = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//1Star//'
    # preprocessing_champ_info(path1, path1_save, 86)


