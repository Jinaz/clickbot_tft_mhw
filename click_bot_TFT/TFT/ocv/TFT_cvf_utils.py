import glob
import keyboard
import time

import pyautogui
import cv2 as cv
import pytesseract
import numpy as np
import matplotlib.pyplot as plt

from skimage import data, img_as_float
from skimage.metrics import structural_similarity as ssim


# Util functions
def screen_shot():
    image = pyautogui.screenshot()
    return image


def cut_img(img, xl, xr, yt, yb):
    image_as_array = np.array(img)
    cut_img = image_as_array[yt:yb, xl:xr, :]
    return cut_img


def color_convert(img):
    return cv.cvtColor(img, cv.COLOR_RGB2BGR)


def save_img(path, filename, img):
    cv.imwrite(path + filename, img)


def ss_save(path, filename):
    # take a screen shot of the screen and store it to the given path
    img = screen_shot()
    image_as_array = np.array(img)
    cvt_img = color_convert(image_as_array)
    save_img(path, filename, cvt_img)


# Function used to load imgs
def load_class(class_name):
    image_list = []

    PATH = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//ClassBased//' + class_name + '/*.png'
    for filename in glob.glob(PATH):
        img = cv.imread(filename)
        image_list.append(img)

    return image_list


def load_imgs(path):
    image_list = []

    for filename in glob.glob(path):
        img = cv.imread(filename)
        image_list.append(img)

    return image_list


def load_imgs_with_name(path, name_length):
    image_dict = dict()

    for filename in glob.glob(path):
        name = filename[name_length:]
        img = cv.imread(filename)
        image_dict[name] = img

    return image_dict


def load_img(path):
    return cv.imread(path)


# Functions that are used to compare two imgs
def mse(imageA, imageB):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err


def compare_images(imageA, imageB, m_th, s_th):
    # compute the mean squared error and structural similarity
    # index for the images
    m = mse(imageA, imageB)
    s = ssim(imageA, imageB, multichannel=True)

    # print(m, s)

    if m <= m_th or s >= s_th:
        # print("m:", m, "s:", s)
        return True
    else:
        return False


def compare_images_and_show(imageA, imageB, title):
    # compute the mean squared error and structural similarity
    # index for the images
    m = mse(imageA, imageB)
    s = ssim(imageA, imageB, multichannel=True)
    # setup the figure
    fig = plt.figure(title)
    plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))
    # show first image
    ax = fig.add_subplot(1, 2, 1)
    plt.imshow(imageA, cmap=plt.cm.gray)
    plt.axis("off")
    # show the second image
    ax = fig.add_subplot(1, 2, 2)
    plt.imshow(imageB, cmap=plt.cm.gray)
    plt.axis("off")
    # show the images
    plt.show()

    return m, s


def compare_images_and_output(imageA, imageB, nameA, nameB):
    # compute the mean squared error and structural similarity
    # index for the images
    m = mse(imageA, imageB)
    s = ssim(imageA, imageB, multichannel=True)
    print(nameA, "and", nameB, ": m-", m, "s-", s)

    return m, s


# functions that are used to help recognize numbers using tessract
def blur_sharpen_img(src, arg='blur'):
    if arg == 'blur':
        kernel = np.ones((9, 9), np.float32) / 81
        dst = cv.filter2D(src, -1, kernel)
        return dst
    else:
        kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
        im = cv.filter2D(src, -1, kernel)
        return im


def rescale(img):
    scale_percent = 300  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv.resize(img, dim, interpolation=cv.INTER_AREA)

    # print('Resized Dimensions : ', resized.shape)

    # cv.imshow("Resized image", resized)
    # cv.waitKey(0)
    return resized


def extend(src):
    print(src.shape)
    value = [0, 0, 0]
    # for 40-90 border of 30 left and top

    outimage = cv.copyMakeBorder(src, 10, 10, 15, 60, cv.BORDER_CONSTANT, None, value)

    # cv.imshow('aaa', outimage)
    # cv.waitKey()

    return outimage


def resize_image(img, scalePercent=300):
    scale_percent = scalePercent  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv.resize(img, dim, interpolation=cv.INTER_AREA)
    return resized


def exam_planning():
    import TFT.play_tft.constants as const

    PATH_p = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Basics//planning.png'
    planing = load_img(PATH_p)
    new_planning_full = load_img('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Basics//0.png')
    new_planning = cut_img(new_planning_full, const.PLANING_SIZE[0], const.PLANING_SIZE[1], const.PLANING_SIZE[2],
                           const.PLANING_SIZE[3])
    compare_images_and_show(planing, new_planning, '')


def exam_champ_info():
    import TFT.play_tft.constants as const
    test = load_img(
        'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//2-Star//2-Janna.png')

    PATH = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champ_Info_Full//test/*.png'
    for filename in glob.glob(PATH):
        img = cv.imread(filename)
        img_cut = cut_img(img, const.CHAMP_INFO_SIZE[0], const.CHAMP_INFO_SIZE[1], const.CHAMP_INFO_SIZE[2],
                          const.CHAMP_INFO_SIZE[3])
        compare_images_and_show(test, img_cut, '')


def determine_champ_info_threshold(PATH='C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//test//*.png'):
    min_m = 10000
    max_s = 0
    for filename1 in glob.glob(PATH):
        for filename2 in glob.glob(PATH):
            name1 = filename1[73:]
            name2 = filename2[73:]
            img1 = cv.imread(filename1)
            img2 = cv.imread(filename2)
            m, s = compare_images_and_output(img1, img2, name1, name2)

            if 1000 < m < min_m:
                min_m = m
            if max_s < s < 0.95:
                max_s = s

    print("s value:", max_s)
    print("m value:", min_m)


def get_champ_info_from_full_pic(FROM, TO):
    import TFT.play_tft.constants as const

    for filename in glob.glob(FROM):
        name = filename[92:]
        print(name)
        img = cv.imread(filename)
        champ_info = cut_img(img, const.CHAMP_INFO_SIZE[0], const.CHAMP_INFO_SIZE[1], const.CHAMP_INFO_SIZE[2],
                             const.CHAMP_INFO_SIZE[3])

        save_img(TO, name, champ_info)


if __name__ == "__main__":
    # get_champ_info_from_full_pic('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champ_Info_Full//2-Star/*.png', 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champ_Info//')

    # img1 = load_img('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//CostBased//3//Kalista.png')
    # for filename in glob.glob(
    #         'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current//*.png'):
    #     img = cv.imread(filename)
    #     compare_images_and_show(img1, img, '')

    # exam_planning()

    exam_champ_info()
    # determine_champ_info_threshold()