import time
from datetime import timedelta
import numpy as np
import keyboard
import pytesseract

import TFT.utils.MouseControl as mc
from TFT.utils.MouseConnectors import Mouse
import TFT.utils.ProcessChecker as ProcessChecker

import TFT.ocv.TFT_cvf_utils as cv_util
import TFT.ocv.TFT_cvf as cvf

import TFT.play_tft.actions as act
import TFT.play_tft.round_actions as ract
import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const

if __name__ == "__main__":

    pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
    curr = cv_util.load_imgs_with_name(
        'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current//*.png', 76)
    one_star = cv_util.load_imgs_with_name(
        'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current_1//*.png', 78)
    two_star = cv_util.load_imgs_with_name(
        'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current_2//*.png', 78)
    stars = dict(one_star, **two_star)

    print(str(len(curr.keys())) + ' champs loaded')
    print(str(len(stars.keys())) + ' champ info loaded')

    PATH_l = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Basics//loading.png'
    loading = cv_util.load_img(PATH_l)
    PATH_p = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Basics//planning.png'
    planing = cv_util.load_img(PATH_p)

    m = Mouse()
    mc.alt_tab()
    program_end = False

    for i in range(100):
        mc.mouse_click(m, True, coord.FIND_MATCH)
        while ProcessChecker.CheckforLeague() is False:
            mc.mouse_click(m, True, coord.FIND_MATCH)
            time.sleep(1)
            mc.mouse_click(m, True, coord.ACCEPT_BUTTON)
            time.sleep(1)

            if keyboard.is_pressed('space'):
                program_end = True
                print("manually stops")
                break

        if program_end:
            break

        print("loading game....")
        start_time = time.time()

        ######################initiate##########################
        is_end = False
        manually_end = False
        round_count = 0
        curr_champ_a = 0
        curr_champ_c = 0
        cur_level = 3
        no_limit = None
        champ_status = dict()
        for k in curr.keys():
            champ_status[k] = 0

        # print(champ_status)
        #######################################################
        time.sleep(10)
        mc.mouse_click(m, True, coord.ACCEPT_BUTTON)
        while cvf.is_loading(loading):
            pass
        print("game loaded: ", str(timedelta(seconds=(time.time() - start_time))))

        start_time = time.time()
        while ProcessChecker.CheckforLeague():

            while not cvf.is_planing(planing):
                if keyboard.is_pressed('space'):
                    is_end = True
                    manually_end = True
                    print("manually stops")
                    break

                if keyboard.is_pressed('p'):
                    round_count += 1
                    act.print_round(round_count)
                    time.sleep(0.5)

                if keyboard.is_pressed('o'):
                    round_count -= 1
                    act.print_round(round_count)
                    time.sleep(0.5)

                if round_count > 12:
                    act.click_exit(m)

                if not ProcessChecker.CheckforLeague():
                    is_end = True
                    break

            if is_end:

                if manually_end:
                    program_end = True

                break

            round_count += 1

            if round_count < 3:  # first 3 rounds with minions
                champ_status, curr_champ_a, curr_champ_c, cur_level = ract.round1(m, round_count, curr, stars,
                                                                                  0, champ_status, curr_champ_a,
                                                                                  curr_champ_c, cur_level)
            elif round_count < 8:  # round 2-1 to 2-7
                champ_status, curr_champ_a, curr_champ_c, cur_level = ract.round2(m, round_count, curr, stars,
                                                                                  0, champ_status, curr_champ_a,
                                                                                  curr_champ_c, cur_level)
            elif round_count < 13:  # round 3-1 to 3-7
                cur_level = 5
                champ_status, curr_champ_a, curr_champ_c, cur_level = ract.round3(m, round_count, curr, stars,
                                                                                  2, champ_status, curr_champ_a,
                                                                                  curr_champ_c, cur_level)
            elif round_count < 18:  # round 4-1 and 4-7
                champ_status, curr_champ_a, curr_champ_c, cur_level = ract.round4(m, round_count, curr, stars, 2,
                                                                                  champ_status,
                                                                                  curr_champ_a, curr_champ_c, cur_level)
            else:  # from round 5-1
                if not no_limit:
                    all = True
                    for k in curr.keys():
                        if champ_status[k] < 2:
                            no_limit = False
                            all = False
                            break

                    if all:
                        no_limit = True
                champ_status, curr_champ_a, curr_champ_c, cur_level = ract.round5(m, round_count, curr, stars, 2,
                                                                                  champ_status,
                                                                                  curr_champ_a, curr_champ_c, cur_level,
                                                                                  no_limit)
            # print(champ_status)

        print("game", i + 1, "finished")
        print("game time: ", str(timedelta(seconds=(time.time() - start_time))))
        print("final status:", champ_status)

        if program_end:
            break

        for n in range(4):
            mc.mouse_click(m, True, coord.COLLECT_REWARDS)
            time.sleep(1)

        mc.mouse_click(m, True, coord.REPORT)
        time.sleep(1)
        mc.mouse_click(m, True, coord.REPORT)
        time.sleep(1)
        mc.mouse_click(m, True, coord.PLAY_AGAIN)
        print('you have ', ts.DEACTIVATION_TIME, ' seconds to stop this bot')
        time.sleep(ts.DEACTIVATION_TIME)
