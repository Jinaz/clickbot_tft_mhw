import TFT.utils.MouseControl as mc
from TFT.utils.MouseConnectors import Mouse
import time
from datetime import timedelta
import numpy as np

import TFT.utils.MouseControl as mc
from TFT.utils.MouseConnectors import Mouse
import TFT.utils.ProcessChecker as ProcessChecker

import TFT.ocv.TFT_cvf_utils as cv_util
import TFT.ocv.TFT_cvf as cvf

import TFT.play_tft.actions as act
import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const

import time
from datetime import timedelta

if __name__ == "__main__":

    PATH_l = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Basics//loading.png'
    loading = cv_util.load_img(PATH_l)
    PATH_p = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Basics//planing.png'
    planing = cv_util.load_img(PATH_p)

    m = Mouse()
    mc.alt_tab()

    for i in range(100):
        mc.mouse_click(m, True, coord.FIND_MATCH)
        while ProcessChecker.CheckforLeague() is False:
            mc.mouse_click(m, True, coord.FIND_MATCH)
            time.sleep(1)
            mc.mouse_click(m, True, coord.ACCEPT_BUTTON)
            time.sleep(1)

        print("loading game....")
        start_time = time.time()
        time.sleep(10)
        mc.mouse_click(m, True, coord.ACCEPT_BUTTON)
        while cvf.is_loading(loading):
            pass
        print("game loaded: ", str(timedelta(seconds=(time.time() - start_time))))

        start_time = time.time()
        time.sleep(ts.TIME_UNTIL_FF)
        while ProcessChecker.CheckforLeague():
            act.surrender(m)

        print("game", i + 1, "finished")
        print("game time: ", str(timedelta(seconds=(time.time() - start_time))))

        for n in range(4):
            mc.mouse_click(m, True, coord.COLLECT_REWARDS)
            time.sleep(1)
        mc.mouse_click(m, True, coord.PLAY_AGAIN)
        time.sleep(ts.DEACTIVATION_TIME)
