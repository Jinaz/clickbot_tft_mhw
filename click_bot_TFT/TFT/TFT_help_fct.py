import keyboard

import TFT.utils.MouseControl as mc
from TFT.utils.MouseConnectors import Mouse
import TFT.utils.ProcessChecker as ProcessChecker

import TFT.ocv.TFT_cvf_utils as cv_util

import TFT.play_tft.actions as act
import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const

import time
from datetime import timedelta
import numpy as np


def begin_tft():
    m = Mouse()
    mc.alt_tab()

    mc.mouse_click(m, True, coord.FIND_MATCH)
    while ProcessChecker.CheckforLeague() is False:
        mc.mouse_click(m, True, coord.ACCEPT_BUTTON)
        time.sleep(2)


def buy_champ_of(path):
    curr = cv_util.load_imgs(path)

    m = Mouse()
    mc.alt_tab()

    while True:
        try:  # used try so that if user pressed other than the given key error will not be shown
            if keyboard.is_pressed('space'):
                act.buy_champs_of(curr, m)

            if keyboard.is_pressed('enter'):
                break

        except:  # if user pressed a key other than the given key the loop will break
            break


def save_full_ss(path, filename):
    cv_util.ss_save(path, filename)


if __name__ == "__main__":
    # begin_tft()

    # champ_path = 'C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current//*.png'
    # buy_champ_of(champ_path)
    save_full_ss('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Temp//', '117.png')


