import click_bot_TFT.TFT.utils.KeyboardConnectors as kb
import keyboard

SC_REFRESH = 0x20    # refresh
SC_SELL = 0x12    # sell champ
SC_UPGRADE = 0x21    # upgrade


def key_press(key):
    kb.PressKey(key)
    kb.ReleaseKey(key)


if __name__ == "__main__":
    import time

    while True:
        try:  # used try so that if user pressed other than the given key error will not be shown
            if keyboard.is_pressed('space'):
                key_press(SC_SELL)
                time.sleep(1)

            if keyboard.is_pressed('enter'):
                break

        except:  # if user pressed a key other than the given key the loop will break
            break
