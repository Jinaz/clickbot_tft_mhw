import click_bot_TFT.TFT.utils.KeyboardConnectors as KeyboardConnectors
from click_bot_TFT.TFT.utils.KeyboardConnectors import ESC, ALT, TAB
from click_bot_TFT.TFT.utils.MouseConnectors import Mouse
from pynput.keyboard import Key, Controller
import click_bot_TFT.TFT.play_tft.time_stamps as ts
import time


def mouse_click(mouse, leftbutton, coords):
    time.sleep(ts.MOUSE_SLEEP_TIME)
    mouse.move_mouse(coords)
    time.sleep(ts.MOUSE_SLEEP_TIME)
    if leftbutton:
        mouse.press_button(mouse.get_position(), "left", False)
        time.sleep(ts.MOUSE_WAIT_TIME)
        mouse.press_button(mouse.get_position(), "left", True)
    else:
        mouse.press_button(mouse.get_position(), "right", False)
        time.sleep(ts.MOUSE_WAIT_TIME)
        mouse.press_button(mouse.get_position(), "right", True)
    time.sleep(ts.MOUSE_SLEEP_TIME)


def mouse_drag(mouse, from_coords, to_coords):
    time.sleep(ts.MOUSE_SLEEP_TIME)
    mouse.move_mouse(from_coords)
    time.sleep(ts.MOUSE_SLEEP_TIME)
    mouse.press_button(mouse.get_position(), "left", False)
    time.sleep(ts.MOUSE_WAIT_TIME)
    mouse.move_mouse(to_coords)
    time.sleep(ts.MOUSE_WAIT_TIME)
    mouse.press_button(mouse.get_position(), "left", True)


def move_mouse_to(mouse, coord):
    mouse.move_mouse(coord)


def alt_tab():
    keyboard = Controller()

    time.sleep(ts.MOUSE_SLEEP_TIME)

    keyboard.press(Key.alt)
    time.sleep(ts.MOUSE_WAIT_TIME)
    keyboard.press(Key.tab)

    time.sleep(ts.MOUSE_WAIT_TIME)

    keyboard.release(Key.tab)
    time.sleep(ts.MOUSE_WAIT_TIME)
    keyboard.release(Key.alt)
    time.sleep(ts.MOUSE_SLEEP_TIME)


def smt():
    mouse = Mouse()
    # mouse.click((100, 100), "right")
    mouse.move_mouse((100, 100))
    mouse.click()
    mouse.move_mouse((200, 200))
    time.sleep(1)
    mouse.press_button(mouse.get_position(), "right", False)
    time.sleep(1)
    mouse.press_button(mouse.get_position(), "right", True)
    # time.sleep(2.0)

    # mouse.click((100, 100), "right")


def pressEsc():
    time.sleep(ts.MOUSE_SLEEP_TIME)
    KeyboardConnectors.PressKey(ESC)
    time.sleep(ts.MOUSE_WAIT_TIME)
    KeyboardConnectors.ReleaseKey(ESC)
    time.sleep(ts.MOUSE_SLEEP_TIME)