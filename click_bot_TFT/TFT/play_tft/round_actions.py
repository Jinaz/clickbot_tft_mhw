import random
import numpy as np
import time

import TFT.utils.MouseControl as mc
import TFT.utils.KeyboaredControl as kbc

import TFT.ocv.TFT_cvf_utils as cv_util
import TFT.ocv.TFT_cvf as cvf

import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const
import TFT.play_tft.actions as act


def round1(mouse, round_count, selected_champ, selected_champ_star, n_upgeade,
           champ_status, curr_assign_champ, curr_check_champ, current_level):
    # round 1-1 to 1-4 (only 1-3 and 1-4 needs actions)
    print('1 - ' + str(round_count + 2))
    act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
    act.collect_item(mouse)
    act.swap(mouse)
    return champ_status, curr_assign_champ, curr_check_champ, current_level


def round2(mouse, round_count, selected_champ, selected_champ_star, n_upgeade,
           champ_status, curr_assign_champ, curr_check_champ, current_level):
    if round_count < 6:  # round 2-1 to 2-3
        print('2 - ' + str(round_count - 2))
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, 3)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, 3)
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.collect_item(mouse)
        act.swap(mouse)

    elif round_count == 6:  # round 2-5
        print('2 - 4')
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, 4)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, 4)
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.collect_item(mouse)
        act.swap(mouse)
        print('2 - 5')

    elif round_count == 7:  # round 2-6 and 2-7
        print('2 - 6')

        start_time = time.time()
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, 4)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, 4)
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.collect_item(mouse)
        act.swap(mouse)
        act.force_upgrade(1)
        time_taken = time.time() - start_time

        if time_taken < ts.ROUND_WAIT_TIME:
            time.sleep(ts.ROUND_WAIT_TIME - time_taken)

        print('2 - 7')
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        time.sleep(ts.BUY_INTERVAL)
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.swap(mouse)

    return champ_status, curr_assign_champ, curr_check_champ, current_level


def round3(mouse, round_count, selected_champ, selected_champ_star, n_upgeade,
           champ_status, curr_assign_champ, curr_check_champ, current_level):
    if round_count < 11:  # round 3-1 to 3-3
        print('3 - ' + str(round_count - 7))
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.upgrade(0, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.random_collect_item(mouse)

    elif round_count == 11:  # round 3-5
        print('3 - 4')
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.random_collect_item(mouse)
        print('3 - 5')

    elif round_count == 12:  # round 2-6 and 2-7
        print('3 - 6')
        start_time = time.time()
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.random_collect_item(mouse)
        time_taken = time.time() - start_time
        if time_taken < ts.ROUND_WAIT_TIME:
            time.sleep(ts.ROUND_WAIT_TIME - time_taken)

        print('3 - 7')
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        time.sleep(ts.BUY_INTERVAL)
        act.buy_champs_with_status(mouse, selected_champ, champ_status, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.swap(mouse)

    return champ_status, curr_assign_champ, curr_check_champ, current_level


def round4(mouse, round_count, selected_champ, selected_champ_star, n_upgeade,
           champ_status, curr_assign_champ, curr_check_champ, current_level):
    if round_count < 16:  # round 4-1 to 4-3
        print('4 - ' + str(round_count - 12))
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

        act.refresh_and_buy(mouse, selected_champ, champ_status, current_level, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.random_collect_item(mouse)

    elif round_count == 16:  # round 4-5
        print('4 - 4')
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

        act.refresh_and_buy(mouse, selected_champ, champ_status, current_level, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.random_collect_item(mouse)
        print('4 - 5')

    elif round_count == 17:  # round 4-6 and 4-7
        print('4 - 6')
        start_time = time.time()
        mc.mouse_click(mouse, False, coord.ROUTE2[0])
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                         curr_check_champ, current_level)
        curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

        act.refresh_and_buy(mouse, selected_champ, champ_status, current_level, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.random_collect_item(mouse)
        time_taken = time.time() - start_time
        if time_taken < ts.ROUND_WAIT_TIME:
            time.sleep(ts.ROUND_WAIT_TIME - time_taken)

        print('4 - 7')
        act.refresh_and_buy(mouse, selected_champ, champ_status, current_level, False)
        act.upgrade(n_upgeade, current_level)
        temp = cvf.get_level()
        if 0 < temp <= 9:
            current_level = temp
        act.swap(mouse)

    return champ_status, curr_assign_champ, curr_check_champ, current_level


def round5(mouse, round_count, selected_champ, selected_champ_star, n_upgeade,
           champ_status, curr_assign_champ, curr_check_champ, current_level, no_limit):
    temp = round_count - 17
    part1 = int(temp / 6)
    part2 = temp - 6 * part1
    if part2 == 0:
        part2 = 6
        part1 -= 1
    print(str(part1 + 5) + ' - ' + str(part2))

    mc.mouse_click(mouse, False, coord.ROUTE2[0])
    curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                     curr_check_champ, current_level)
    curr_check_champ, champ_status = act.check_champ_info_with_state(mouse, selected_champ_star, champ_status,
                                                                     curr_check_champ, current_level)
    curr_assign_champ = act.assign_item(mouse, curr_assign_champ, current_level)

    act.refresh_and_buy(mouse, selected_champ, champ_status, current_level, no_limit)
    act.upgrade(n_upgeade, current_level)
    temp = cvf.get_level()
    if 0 < temp <= 9:
        current_level = temp
    act.random_collect_item(mouse)

    return champ_status, curr_assign_champ, curr_check_champ, current_level
