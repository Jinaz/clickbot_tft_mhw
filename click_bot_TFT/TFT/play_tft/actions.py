import random
import numpy as np
import time

import TFT.utils.MouseControl as mc
import TFT.utils.KeyboaredControl as kbc

import TFT.ocv.TFT_cvf_utils as cv_util
import TFT.ocv.TFT_cvf as cvf

import TFT.play_tft.coordinates as coord
import TFT.play_tft.time_stamps as ts
import TFT.play_tft.constants as const


def print_round(round_count):
    if round_count < 3:  # first 3 rounds with minions
        print('1 - ' + str(round_count + 2))
    elif round_count < 6:  # round 2-1 to 2-3
        print('2 - ' + str(round_count - 2))
    elif round_count == 6:  # round 2-5
        print('2 - 4')
        print('2 - 5')
    elif round_count == 7:  # round 2-6 and 2-7
        print('2 - 6')
        print('2 - 7')
    elif round_count < 11:  # round 3-1 to 3-3
        print('3 - ' + str(round_count - 7))
    elif round_count == 11:  # round 3-5
        print('3 - 4')
        print('3 - 5')
    elif round_count == 12:  # round 2-6 and 2-7
        print('3 - 6')
        print('3 - 7')
    elif round_count < 16:  # round 4-1 to 4-3
        print('4 - ' + str(round_count - 12))
    elif round_count == 16:  # round 4-5
        print('4 - 4')
        print('4 - 5')
    elif round_count == 17:  # round 4-6 and 4-7
        print('4 - 6')
        print('4 - 7')
    else:  # from round 5-1
        temp = round_count - 17
        part1 = int(temp / 6)
        part2 = temp - 6 * part1
        if part2 == 0:
            part2 = 6
            part1 -= 1
        print(str(part1 + 5) + ' - ' + str(part2))


def surrender(mouse):
    # ff sequence after waiting
    mc.pressEsc()
    # true left else right
    mc.mouse_click(mouse, True, coord.FF_BUTTON)
    mc.mouse_click(mouse, True, coord.CONFIRM_BUTTON)


def buy_random_champ(mouse):
    if random.randint(0, 9) >= 5:
        champ = random.randint(0, 4)
        mc.mouse_click(mouse, True, coord.CHAMPS[champ])


def buy_champs_of(mouse, selected_champs):
    champs = cvf.get_index(selected_champs)

    if len(champs) > 0:
        print('buying champ...')
        for i in champs:
            mc.mouse_click(mouse, True, coord.CHAMPS[i])


def buy_3champs_of(mouse, selected_champs, n_champ_bought):
    champs = cvf.get_champs()
    before_buy = np.zeros(len(selected_champs))
    after_buy = np.zeros(len(selected_champs))

    have_bought = False
    for i in range(len(champs)):
        for j in range(len(selected_champs)):
            if cv_util.compare_images(champs[i], selected_champs[j], const.BUY_CHAMP_M_TH, const.BUY_CHAMP_S_TH):
                before_buy[j] += 1
                if n_champ_bought[j] < 3:
                    print('buying champ...')
                    mc.mouse_click(mouse, True, coord.CHAMPS[i])
                    have_bought = True

    if have_bought:
        champs = cvf.get_champs()
        for i in range(len(champs)):
            for j in range(len(selected_champs)):
                if cv_util.compare_images(champs[i], selected_champs[j], const.BUY_CHAMP_M_TH, const.BUY_CHAMP_S_TH):
                    after_buy[j] += 1

        n_champ_bought += before_buy - after_buy

    # print(n_champ_bought)
    return n_champ_bought


def buy_champs_with_status(mouse, selected_champs, champ_status, no_limit):
    champs = cvf.get_champs()
    # champs = cvf.load_champs_from_data('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Champ_Info_Full//2-Star/2-Janna.png')

    for i in range(len(champs)):
        for champ in selected_champs:
            pic = selected_champs[champ]
            if cv_util.compare_images(champs[i], pic, const.BUY_CHAMP_M_TH, const.BUY_CHAMP_S_TH):
                if no_limit or champ_status[champ] < 2:
                    print('buying champ:', champ[:-4])
                    mc.mouse_click(mouse, True, coord.CHAMPS[i])


def swap(mouse):
    for i in range(0, len(coord.CANDIDATES) - 1):
        mc.mouse_drag(mouse, coord.CANDIDATES[i + 1], coord.CANDIDATES[i])


def collect_item(mouse):
    for p in coord.ROUTE1:
        mc.mouse_click(mouse, False, p)
        time.sleep(2)
    for i in range(3):
        mc.mouse_click(mouse, False, coord.ROUTE2[i])
        time.sleep(2)
    mc.mouse_click(mouse, False, coord.ROUTE2[3])


def random_collect_item(mouse):
    if random.randint(0, 9) >= 5:
        for i in range(len(coord.ROUTE1) - 1):
            mc.mouse_click(mouse, False, coord.ROUTE1[i])
            time.sleep(2)
        mc.mouse_click(mouse, False, coord.ROUTE1[len(coord.ROUTE1) - 1])
    else:
        for i in range(len(coord.ROUTE2) - 1):
            mc.mouse_click(mouse, False, coord.ROUTE2[i])
            time.sleep(2)
        mc.mouse_click(mouse, False, coord.ROUTE2[len(coord.ROUTE2) - 1])


def assign_item(mouse, index, cap):
    for i in range(5):
        mc.mouse_drag(mouse, coord.ITEMS[i], coord.CHAMPS_ON_BOARD[index])

    index += 1
    if index == cap:
        index = 0

    return index


def upgrade(n_upgeade, level):
    if level < 9:
        coins = cvf.get_coins()
        if coins == -1:
            for i in range(n_upgeade):
                kbc.key_press(kbc.SC_UPGRADE)
        elif coins > 50:
            num = int((coins - 50) / 4)
            for i in range(num):
                kbc.key_press(kbc.SC_UPGRADE)


def force_upgrade(n_upgeade):
    for i in range(n_upgeade):
        kbc.key_press(kbc.SC_UPGRADE)


def refresh():
    kbc.key_press(kbc.SC_REFRESH)
    time.sleep(0.5)


def refresh_and_buy(mouse, selected_champs, champ_status, level, no_limit):

    buy_champs_with_status(mouse, selected_champs, champ_status, no_limit)
    if level == 7:
        refresh()
        buy_champs_with_status(mouse, selected_champs, champ_status, no_limit)
    elif level == 8:
        refresh()
        buy_champs_with_status(mouse, selected_champs, champ_status, no_limit)
        refresh()
        buy_champs_with_status(mouse, selected_champs, champ_status, no_limit)
    elif level == 9:
        while cvf.get_coins() > 30:
            refresh()
            buy_champs_with_status(mouse, selected_champs, champ_status, no_limit)


def click_exit(mouse):
    mc.mouse_click(mouse, True, coord.EXIT_NOW)
    # print("trying to click exit now")


def sell_all(mouse):
    for c in coord.CANDIDATES:
        mc.move_mouse_to(mouse, c)
        kbc.key_press(kbc.SC_SELL)


def sell_champ_of(mouse, coords):
    # mc.mouse_drag(mouse, coords, coord.CHAMPS[2])
    mc.move_mouse_to(mouse, coords)
    kbc.key_press(kbc.SC_SELL)


def get_champ_info_of(mouse, index):
    mc.mouse_drag(mouse, coord.CHAMPS_ON_BOARD[index], coord.CHECK_CHAMP)

    mc.mouse_click(mouse, False, coord.CHECK_CHAMP)

    screen_shot = cv_util.screen_shot()
    cut_screen_shot = cv_util.cut_img(screen_shot, const.CHAMP_INFO_SIZE[0], const.CHAMP_INFO_SIZE[1],
                                      const.CHAMP_INFO_SIZE[2], const.CHAMP_INFO_SIZE[3])
    cvt_screen_shot = cv_util.color_convert(cut_screen_shot)
    # mc.mouse_drag(mouse, CHECK_CHAMP, CHAMPS_ON_BOARD[index])

    return cvt_screen_shot


def check_champ_info_of(mouse, selected_champs, index, cap):
    img = get_champ_info_of(mouse, index)
    wanted = False
    for pic in selected_champs:
        if cv_util.compare_images(img, pic, const.CHECK_CHAMP_M_TH, const.CHECK_CHAMP_S_TH):
            wanted = True
            mc.mouse_drag(mouse, coord.CHECK_CHAMP, coord.CHAMPS_ON_BOARD[index])
            break

    if not wanted:
        sell_champ_of(mouse, coord.CHECK_CHAMP)
        index -= 1
        print('sell unwanted champ')

    index += 1
    if index == cap:
        index = 0

    return index


def check_champ_info_with_state(mouse, selected_champs, champ_status, index, cap):
    img = get_champ_info_of(mouse, index)

    wanted = False
    champ = None
    old_status = -1
    new_status = -1
    for key in selected_champs:
        pic = selected_champs[key]
        if cv_util.compare_images(img, pic, const.CHECK_CHAMP_M_TH, const.CHECK_CHAMP_S_TH):
            champ = key[2:]
            old_status = champ_status[champ]
            new_status = int(key[0])
            if new_status >= old_status:
                wanted = True
                champ_status[champ] = new_status
            break

    if cap >= 5 and not wanted:     # sell champ only when reached level 5
        sell_champ_of(mouse, coord.CHECK_CHAMP)
        index -= 1
        if champ:
            print('sell unwanted champ: level', new_status, champ[:-4])
        else:
            print('sell unwanted champ')
    else:
        if champ:
            print('current champ: level', new_status, champ[:-4])
        else:
            print('current champ: others')
        mc.mouse_drag(mouse, coord.CHECK_CHAMP, coord.CHAMPS_ON_BOARD[index])

    index += 1
    if index == cap:
        index = 0

    return index, champ_status


if __name__ == "__main__":
    curr = cv_util.load_imgs_with_name('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current//*.png', 76)
    # one_star = cv_util.load_imgs_with_name('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current_1//*.png', 78)
    # two_star = cv_util.load_imgs_with_name('C://Workspace//click_bot//clickbot_tft_mhw//click_bot_TFT//DATA_4k//Current_2//*.png', 78)
    # stars = dict(one_star, **two_star)
    #
    champ_status = dict()
    #
    for k in curr.keys():
        champ_status[k] = 2

    print("final status:", champ_status)
    #
    # print(champ_status)
    #
    # index, champ_status = check_champ_info_with_state('', stars, champ_status, 0, 10)
    #
    # print(champ_status)

    # no_limit = None
    # if not no_limit:
    #     all = True
    #     for k in curr.keys():
    #         if champ_status[k] < 2:
    #             no_limit = False
    #             all = False
    #             break
    #
    #     if all:
    #         no_limit = True
    #
    # print(no_limit)

    # buy_champs_with_status('', curr, champ_status, no_limit)
