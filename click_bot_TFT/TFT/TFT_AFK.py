import datetime
import time
import threading
import keyboard

import click_bot_TFT.TFT.utils.ProcessChecker as ProcessChecker
import click_bot_TFT.TFT.utils.KeyboardConnectors as KeyboardConnectors
from pynput.keyboard import Key, Controller
from click_bot_TFT.TFT.utils.KeyboardConnectors import ESC, ALT, TAB
from click_bot_TFT.TFT.utils.MouseConnectors import Mouse

ACCEPTBUTTON = (958, 725)
FINDMATCH = (855, 850)
PLAYAGAIN = (855, 843)
COLLECTREWARDS = (960, 850)

FFBUTTON = (760, 870)
CONFIRMBUTTON = (855, 605)
EXITNOW = (830, 530)

SLEEPTIMER = 0.3
WAITTIMER = 0.1
TIMEUNTILFF = 900
DEACTIVATIONTIMER = 6

mouse = Mouse()


class Parameter(object):
    pass


def mouseclick(mouse, button, coords):
    time.sleep(SLEEPTIMER)
    mouse.move_mouse(coords)
    time.sleep(SLEEPTIMER)
    if button:
        mouse.press_button(mouse.get_position(), "left", False)
        time.sleep(WAITTIMER)
        mouse.press_button(mouse.get_position(), "left", True)
    else:
        mouse.press_button(mouse.get_position(), "right", False)
        time.sleep(WAITTIMER)
        mouse.press_button(mouse.get_position(), "right", True)
    time.sleep(SLEEPTIMER)


def alttab():
    keyboard = Controller()

    time.sleep(1)

    keyboard.press(Key.alt)
    time.sleep(0.1)
    keyboard.press(Key.tab)

    time.sleep(0.1)

    keyboard.release(Key.tab)
    time.sleep(0.1)
    keyboard.release(Key.alt)
    time.sleep(1)


def smt():
    mouse = Mouse()
    # mouse.click((100, 100), "right")
    mouse.move_mouse((100, 100))
    mouse.click()
    mouse.move_mouse((200, 200))
    time.sleep(1)
    mouse.press_button(mouse.get_position(), "right", False)
    time.sleep(1)
    mouse.press_button(mouse.get_position(), "right", True)
    # time.sleep(2.0)

    # mouse.click((100, 100), "right")


def pressEsc():
    time.sleep(SLEEPTIMER)
    KeyboardConnectors.PressKey(ESC)
    time.sleep(WAITTIMER)
    KeyboardConnectors.ReleaseKey(ESC)
    time.sleep(SLEEPTIMER)


def surrender(mouse):
    # ff sequence after waiting
    pressEsc()
    # true left else right
    mouseclick(mouse, True, FFBUTTON)
    mouseclick(mouse, True, CONFIRMBUTTON)


def playTFT(params):
    param_copy = None
    with params.lock:
        param_copy = params

    alttab()
    while True:

        with params.lock:
            if params.has_changed:
                param_copy = params
                params.has_changed = False

        print("Game", param_copy.counter)
        while ProcessChecker.CheckforLeague() is False:
            mouseclick(mouse, True, FINDMATCH)
            mouseclick(mouse, True, ACCEPTBUTTON)

        start = datetime.datetime.now()
        time.sleep(param_copy.SLEEPTIME)
        while ProcessChecker.CheckforLeague():
            mouseclick(mouse, True, EXITNOW)
            time.sleep(0.5)

        time.sleep(2)
        end = datetime.datetime.now()
        print("Game finished:", (end - start))

        for n in range(4):
            mouseclick(mouse, True, COLLECTREWARDS)

        mouseclick(mouse, True, PLAYAGAIN)
        time.sleep(param_copy.DEACTIVATIONTIMER)

        with params.lock:
            params.counter += 1


if __name__ == "__main__":

    parameter = Parameter()
    parameter.has_changed = False
    parameter.lock = threading.Lock()
    parameter.SLEEPTIME = TIMEUNTILFF
    parameter.DEACTIVATIONTIMER = DEACTIVATIONTIMER
    parameter.counter = 1

    playTFTthread = threading.Thread(target=playTFT, args=(parameter,), daemon=True)
    playTFTthread.start()

    while True:
        if keyboard.is_pressed('space'):
            print("Manually stopped")
            break

